<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/features", name="features")
     */
    public function features()
    {
        return $this->render('features.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/impressum", name="impressum")
     */
    public function imprint()
    {
        return $this->render('imprint.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/datenschutz", name="datenschutz")
     */
    public function privacy()
    {
        return $this->render('privacy.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
